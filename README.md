## BEACON DATABASE

This folder is the beacon object database. It define all available objects on sessions and sessions themselves.

This database should be in /users/blissadm/local/beamline_configuration on the BEACON_HOST

The objects are defined in YAML (.yml) files, and organized on folder per types.

### Hints

 - To add an object to the database, add the Yaml definition in an .yml file, somewhere in this folder. The object must have the "name" property. 
 - To add the object name to the list of config-objects on the session you need it.

