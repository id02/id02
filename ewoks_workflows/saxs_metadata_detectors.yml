###
### These parameter sets are used for EWOKS workflow post processing.
### Parameters sent to LIMA are a combination of lima_ and processing_ dataset whereas parameters sent to workflow are only the processing_ ones.
### Multiple parameter sets may be defined for each purpose. They are then combined before sending them to the devices (LIMA or workflow).
### Dynamic parameters might be set in the session startup script. See sessions/scripts/ewoks_dynamic_parameters.py
###

##########################################
##                                      ##
##    COMMON                            ##
##                                      ##
##########################################

- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_common_config
  detectors:
    - eiger2
    - eiger500k
    - waxs
    - bv
  static:
    SaxsDataVersion: "bliss_v0.1"
    ExperimentInfo: "-"
    ProposalInfo: "blc0000"
#    WaveLength: 1.01377e-10
#    ScalersFileName: "gCNfinal-r_scalers_00000_raw.h5"                        
#    ScalersFilePath: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-20230903-234141"   


##########################################
##                                      ##
##    SAXS                              ##
##                                      ##
##########################################

# This are the common parameters for all SAXS detectors, to be considered as a LIMA common_header.
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_saxs_config
  detectors:
    - eiger2
    - eiger500k
    - bv
  static:
    UserSampleOffset: .29
    CalibSampleOffset: -.0213
#    DetectorPosition: -14.7319 
    

##########################################
##                                      ##
##    EIGER2                            ##
##                                      ##
##########################################

# This are the processing parameters of EIGER2-4M.
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: processing_eiger2_config
  detectors:
    - eiger2
  static:
    flat_filename: /data/id02/archive/setup/spatcorr-files/eiger2/flat_eiger2_1b1.edf
    npt1_rad: 1000
    'npt1_rad_log10(q.m)_None': 350
    npt2_azim: 360
    npt2_rad: 1000
#    regrouping_mask_filename: "/data/visitor/ch6827/id02/config/mask-1p3m_eiger2.edf"
    scaling_factor: 1.33
    to_save: " norm azim ave"
    unit: "q_nm^-1"
    variance_formula: "0.5+(1/1)*(data-dark)"

# This is EIGER2-specific parameters to be considered as a LIMA common_header only
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_eiger2_config
  detectors:
    - eiger2
  static:
#    BSize_1: 1                                                                
#    BSize_2: 1                                                                
    Center_1: None                                                     
    Center_2: None                                                        
    DDummy: 0.1                                                               
#    DarkFilePath: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"
    DatLabel: "Iq"                                                            
    DetboxOffset: 15.4587                                                     
    DetectorName: "eiger2"                                                     
    DetectorRotation_1: 0                                                     
    DetectorRotation_2: 0                                                     
    DetectorRotation_3: 0                                                     
    Dim_1: 2068                                                               
    Dim_2: 2162                                                               
    DistortionFileName: "0"                                                   
    DistortionFilePath: "/data/id02/archive/setup/spatcorr-files/eiger2"      
    Dummy: -10                                                                
    FlatfieldFileName: "flat_eiger2_1b1.edf"                                  
    FlatfieldFilePath: "/data/id02/archive/setup/spatcorr-files/eiger2"       
    HMRunNumber: 0                                                            
    MaskFileName: "mask-1p3m_eiger2.edf"                                      
    MaskFilePath: "/data/visitor/ch6827/id02/config"                          
    NormalizationFactor: 1.33                                                 
    Offset_1: 0                                                               
    Offset_2: 0                                                               
    PSize_1: 7.5e-05                                                          
    PSize_2: 7.5e-05                                                          
    RasterOrientation: 1                                                      
#    SampleDistance: 1.0168                                                
    ShutterTimeCorrection: 0 


##########################################
##                                      ##
##    EIGER500k                         ##
##                                      ##
##########################################

# This are the processing parameters of EIGER2-500k-PSI (for DAHU).
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: processing_eiger500k_config
  detectors:
    - eiger500k
  static:
    flat_filename: /data/id02/archive/setup/spatcorr-files/eiger500k/flat_eiger500k_1b1_masked.edf
    npt1_rad: 1000
    'npt1_rad_log10(q.m)_None': 350
    npt2_azim: 360
    npt2_rad: 1000
#    regrouping_mask_filename: "/data/visitor/ch6827/id02/config/mask-1p3m_eiger2.edf"
    scaling_factor: 1.33
    to_save: " norm azim ave"
    unit: "q_nm^-1"
    variance_formula: "0.5+(1/1)*(data-dark)"

# This is EIGER500k-specific parameters to be considered as a LIMA common_header only
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_eiger500k_config
  detectors:
    - eiger500k
  static:
#    BSize_1: 1                                                                
#    BSize_2: 1                                                                
    Center_1: None                                                     
    Center_2: None                                                        
    DDummy: 0.1                                                               
#    DarkFilePath: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"
    DatLabel: "Iq"                                                            
    DetboxOffset: 15.4587                                                     
    DetectorName: "eiger500k"                                                     
    DetectorRotation_1: 0                                                     
    DetectorRotation_2: 0                                                     
    DetectorRotation_3: 0                                                     
    Dim_1: 2068                                                               
    Dim_2: 2162                                                               
    DistortionFileName: "0"                                                   
    DistortionFilePath: "/data/id02/archive/setup/spatcorr-files/eiger500k"      
    Dummy: -10                                                                
    FlatfieldFileName: "flat_eiger500k_1b1_masked.edf"                                  
    FlatfieldFilePath: "/data/id02/archive/setup/spatcorr-files/eiger500k"       
    HMRunNumber: 0                                                            
    MaskFileName: "mask-1p3m_eiger2.edf"                                      
    MaskFilePath: "/data/visitor/ch6827/id02/config"                          
    NormalizationFactor: 1.33                                                 
    Offset_1: 0                                                               
    Offset_2: 0                                                               
    PSize_1: 7.5e-05                                                          
    PSize_2: 7.5e-05                                                          
    RasterOrientation: 1                                                      
#    SampleDistance: 1.0168                                                
    ShutterTimeCorrection: 0 

##########################################
##                                      ##
##    BV                                ##
##                                      ##
##########################################

# This are the processing parameters of WAXS
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: processing_bv_config
  detectors:
    - bv
  static:
    flat_filename: ""
    scaling_factor: 1.0
    to_save: " sub"
    variance_formula: "0.5+(1/1)*(data-dark)"
    dark_filter: "quantil"
    dark_filter_quantil: 0.5
    dark_filter_quantil_lower: 0.1
    dark_filter_quantil_upper: 0.9
    do_polarization: 0
    
# This is BV-specific parameters to be considered as a LIMA common_header only
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_bv_config
  detectors:
    - bv
  static:
    BSize_1: 1                                                                
    BSize_2: 1                                                                
    Center_1: 20                                                    
    Center_2: 30
    PSize_1: 4.4e-06                                                          
    PSize_2: 4.4e-06 
    DDummy: 0.1                                                               
    DarkFilePath: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"
    DarkFileName: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"


##########################################
##                                      ##
##    WAXS                              ##
##                                      ##
##########################################

# This are the processing parameters of WAXS
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: processing_waxs_config
  detectors:
    - waxs
  static:
    flat_filename: /data/id02/archive/setup/spatcorr-files/waxs/flat_waxs_2b2.edf
    npt1_rad: 1000
    'npt1_rad_log10(q.m)_None': 350
    npt2_azim: 360
    npt2_rad: 1000
#    regrouping_mask_filename: "/data/visitor/ch6827/id02/config/mask-1p3m_eiger2.edf"
    scaling_factor: 0.36
    to_save: " norm azim ave"
    unit: "q_nm^-1"
    variance_formula: "0.5+(1/1)*(data-dark)"
    dark_filter: "quantil"
    dark_filter_quantil: 0.5
    dark_filter_quantil_lower: 0.1
    dark_filter_quantil_upper: 0.9
    do_polarization: 1
    

# This is WAXS-specific parameters to be considered as a LIMA common_header only
- class: ID02ParameterSet
  plugin: bliss
  package: id02.acquisition.parameter_set
  name: lima_waxs_config
  detectors:
    - waxs
  static:
#    BSize_1: 1                                                                
#    BSize_2: 1                                                                
    Center_1: None                                                     
    Center_2: None                                                        
    DDummy: 0.1                                                               
    DarkFilePath: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"
    DarkFileName: "/data/visitor/ch6827/id02/gCNfinal_r/gCNfinal_r_data-202309"
    DatLabel: "Iq"                                                            
    DetboxOffset: 15.4587   
    DetectorPosition: -14.5485
    DetectorName: "waxs"                                                     
    DetectorRotation_1: 0                                                     
    DetectorRotation_2: 0                                                     
    DetectorRotation_3: 0                                                     
#    Dim_1: 3840                                                               
#    Dim_2: 1920                                                               
    DistortionFileName: "0"                                                   
    DistortionFilePath: "/data/id02/archive/setup/spatcorr-files/waxs"      
    Dummy: -10                                                                
    FlatfieldFileName: "flat_waxs_2b2.edf"                                  
    FlatfieldFilePath: "/data/id02/archive/setup/spatcorr-files/waxs"       
    HMRunNumber: 0                                                            
    MaskFileName: "mask-1p3m_eiger2.edf"                                      
    MaskFilePath: "/data/visitor/ch6827/id02/config"                          
    NormalizationFactor: 0.36                                                
    Offset_1: 0                                                               
    Offset_2: 0                                                               
#    PSize_1: 4.4e-05                                                          
#    PSize_2: 4.4e-05                                                          
    RasterOrientation: 1                                                      
    UserSampleOffset: 0.140                                           
    ShutterTimeCorrection: 1
    ShutterOpeningTime: 0.00145
    ShutterClosingTime: 0.00145





