
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")
load_script("attenuators.py")

print("")
print("Welcome to your new 'attenuators' BLISS session !! ")
print("")
print("You can now customize your 'attenuators' session by changing files:")
print("   * /attenuators_setup.py ")
print("   * /attenuators.yml ")
print("   * /scripts/attenuators.py ")
print("")


attmenu = ID02AttenuatorMenu()


