
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")

ACTIVE_MG = mg_mcs
plotselect('mcs:pin1_raw')
flint()

load_script("dmo.py")

print("")
print("Welcome to your new 'dmo' BLISS session !! ")
print("")
print("You can now customize your 'dmo' session by changing files:")
print("   * /dmo_setup.py ")
print("   * /dmo.yml ")
print("   * /scripts/dmo.py ")
print("")
