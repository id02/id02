
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")

ACTIVE_MG = mg_mcs
plotselect('mcs:pin1_raw')
flint()

load_script("mirror.py")

print("")
print("Welcome to your new 'mirror' BLISS session !! ")
print("")
print("You can now customize your 'mirror' session by changing files:")
print("   * /mirror_setup.py ")
print("   * /mirror.yml ")
print("   * /scripts/mirror.py ")
print("")
