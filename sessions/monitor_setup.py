
from bliss.setup_globals import *
import time

load_script("default_scansaving_homeopid02.py")
load_script("monitor.py")

print("")
print("Welcome to your new 'monitor' BLISS session !! ")
print("")
print("You can now customize your 'monitor' session by changing files:")
print("   * /monitor_setup.py ")
print("   * /monitor.yml ")
print("   * /scripts/monitor.py ")
print("")
