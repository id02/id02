
import os

from bliss.common.standard import * # import all default functions, scans, etc.
from bliss.setup_globals import *


load_script("default_scansaving_homeopid02.py")
load_script("nanoid02.py")


print("")
print("Welcome to 'nanoid02' BLISS session !! ")
print("")
print("This is a test session to run only in CC5.")
