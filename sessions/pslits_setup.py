
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")

ACTIVE_MG = mg_mcs
plotselect('mcs:pin1_raw')
flint()

load_script("pslits.py")

print("")
print("Welcome to your new 'pslits' BLISS session !! ")
print("")
print("You can now customize your 'pslits' session by changing files:")
print("   * /pslits_setup.py ")
print("   * /pslits.yml ")
print("   * /scripts/pslits.py ")
print("")
