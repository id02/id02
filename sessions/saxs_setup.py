
from bliss.setup_globals import *

load_script("saxs.py")
load_script("userfun.py")
load_script("ewoks_dynamic_parameters.py")
load_script("detectors_setup.py")
load_script("instrument_meta.py")
load_script("configdump.py")

DEFAULT_CHAIN.set_settings(default_chain_tfg['chain_config'])

detectors_setup()
