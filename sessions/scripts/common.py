# -*- coding: utf-8 -*-

"""
The ID02 common commands to all sessions initialization
"""

import os
from id02.common.shutters import so, sc, bvop, bvcl, feopen, feclose, fson, fsoff, fsstate
from id02.common.specmot_utils import *
from id02.common.user_macros import *
