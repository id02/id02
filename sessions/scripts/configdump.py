import os
from bliss.config.static import get_config

_DUMPFILE_FOLDER = "/users/opid02/det-files/"
_NDUMPFILE = 40

def ddodump(detector_name, config_name, force=False):

    cfg = get_config()

    avail_detectors = ('eiger2',)

    if detector_name not in avail_detectors:
        raise ValueError(f"Detector {detector_name} is not supported. Available detectors: {avail_detectors}")

    
    limacfg = cfg.get(f"lima_{detector_name}_config")
    limafields = ('Center_1','Center_2','MaskFilePath','MaskFileName')

    motors = [('detbox',),('bsh','bsv'),('dettabv','dettabh')]
    motors2 = [('frameh','framev'),]

    framebs = cfg.get('framebs')

    os.makedirs(_DUMPFILE_FOLDER, exist_ok=True)

    filename = f"{_DUMPFILE_FOLDER}/{config_name}_{detector_name}.py"

    if os.path.isfile(filename) and not force:
        raise ValueError("Dump file with same name already exist!")

    lines = ['sc()', '', *trm_counter.dump().split('\n'), '' ]

    for MM in motors:
        mp = {}
        for M in MM:
            mot = cfg.get(M)
            mp[M] = mot.dial

        lines += [ "umvd("+', '.join([f'{k}, {v}' for k, v in mp.items()])+")" ]

    lines += [ '', ]

    frstat = framebs.framestatus()
    framesin = []

    for k,v in frstat.items():
        if v:
            framesin += [ f"{k}" ]

    if len(framesin) == 0:
        lines += [ f"framebs.frameout()" ]
    else:
        lines += [ "framebs.framein("+','.join(framesin)+")" ]
        
        for MM in motors2:
            mp = {}
            for M in MM:
                mot = cfg.get(M)
                mp[M] = mot.dial

            lines += [ "umvd("+', '.join([f'{k}, {v}' for k, v in mp.items()])+")" ]

    lines += [ '', ]

    for f in limafields:
        lines += [ f"{limacfg.name}.setStaticField('{f}', '"+limacfg.getFieldValue(f)+"')" ]

    lines += [ "print('\\n\\n******\\n\\nConfig OK!\\n\\n******')" ]

    with open(filename, 'w') as fd:
        for l in lines:
            fd.write(f"{l}\n")



def ldo():

    a = [os.path.join(_DUMPFILE_FOLDER,s) for s in os.listdir(_DUMPFILE_FOLDER)
         if os.path.isfile(os.path.join(_DUMPFILE_FOLDER, s))]

    a.sort(key=lambda s: os.path.getmtime(s))

    if len(a) > _NDUMPFILE:
        a = a[-_NDUMPFILE:]
    
    for b in a:
        print(f"ddo('{b}')")


def ddo(filename):
    if not os.path.isabs(filename):
        filename = os.path.join(_DUMPFILE_FOLDER, filename)

    do(filename)
