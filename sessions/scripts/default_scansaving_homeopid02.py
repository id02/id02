

print("*****************\nSaving scans to default location.\n*****************\n\n")

SCAN_SAVING.base_path = "/users/opid02/local/bliss_scans/{session}/"
SCAN_SAVING.writer = "nexus"
SCAN_SAVING.template = ""
SCAN_SAVING.images_path_template = ""
SCAN_SAVING.date_format = "%Y%m%d"
SCAN_SAVING.data_filename = "{session}_{date}"

print(SCAN_SAVING.__info__())
