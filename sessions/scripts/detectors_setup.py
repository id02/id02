
def detectors_setup():
    
    try:
        eiger2.roi_counters['stats'] = eiger2.image.roi
    
        eiger2.camera.threshold_energy = 6200
        eiger2.camera.photon_energy = 12400
        
        eiger2.processing.use_mask = True
        eiger2.processing.mask = '/data/id02/archive/setup/spatcorr-files/eiger2/eiger2_hotmask.edf'
        
        # DO NOT ENABLE: This will replace the default flatfield from DECTRIS and mess up the measurements
        #eiger2.processing.use_flatfield = True
        #eiger2.processing.flatfield = '/data/id02/archive/setup/spatcorr-files/eiger2/flat_eiger2_1b1.edf'
        
        print("Setup of eiger2: Done")
    except:
        print("Cannot set parameters of eiger2 detector : Probably disabled")
        
    try:
        eiger500k.roi_counters['stats'] = eiger500k.image.roi
    
        eiger500k.camera.threshold_energy = 6200
        #eiger500k.camera.photon_energy = 12400
        
        eiger500k.processing.use_mask = True
        eiger500k.processing.mask = '/data/id02/archive/setup/spatcorr-files/eiger500k/eiger500k_hotmask.edf'
    
        print("Setup of eiger500k: Done")
    except:
        print("Cannot set parameters of eiger500k detector : Probably disabled")
        
        
    try:
        waxs.roi_counters['stats'] = waxs.image.roi
        
        print("Setup of waxs: Done")
    except:
        print("Cannot set parameters of waxs: Probably disabled")
        
    try:
        bv.roi_counters['stats'] = bv.image.roi
        
        print("Setup of bv: Done")
    except:
        print("Cannot set parameters of bv: Probably disabled")
        
        
def detectors_unsetup():
    # Just reset the ROI, hotmask, threshold and photon energy are the same
    
    # eiger2
    try:
        ke = [ k for k in eiger2.roi_counters.keys() ]
        for k in ke:
            del eiger2.roi_counters[k]
    except:
        pass
    
    # eiger500k
    try:
        ke = [ k for k in eiger500k.roi_counters.keys() ]
        for k in ke:
            del eiger500k.roi_counters[k]
    except:
        pass
        
    # waxs
    try:
        ke = [ k for k in waxs.roi_counters.keys() ]
        for k in ke:
            del waxs.roi_counters[k]
    except:
        pass
        
    # bv
    try:
        ke = [ k for k in bv.roi_counters.keys() ]
        for k in ke:
            del bv.roi_counters[k]
    except:
        pass
    
    
        
