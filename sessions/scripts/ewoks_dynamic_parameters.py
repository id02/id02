
# motor positions (detbox, egy)
_wavelength = config.__wrapped__.get('wl')
_detbox = config.__wrapped__.get('detbox')

from typing import Optional

@typechecked
def detx(sdd_in_mm : float =None, do_move : bool =True, quiet : bool =False):
    
    # These values are in meter: convert to mm
    offset          = float(lima_eiger2_config.getFieldValue('DetboxOffset'))*1000
    sampleoffset    = float(lima_saxs_config.getFieldValue('UserSampleOffset'))*1000
    sdd_now = _detbox.position + offset + sampleoffset
    
    if not quiet:
        print(f"Detbox position                          : {_detbox.position:10.3f} mm")
        print(f"Detbox offset                            : {offset:10.3f} mm")
        print(f"Sample offset                            : {sampleoffset:10.3f} mm")
        print(f"Current sample-detector distance (eiger2): {sdd_now:10.3f} mm")
    
    
    if sdd_in_mm is not None:
        # Formula: SampleDistance = detbox.position + detboxoffset + sampleoffset
        # So: detbox.position = sampledistance - detboxoffset - sampleoffset
        
        detpos = sdd_in_mm - offset - sampleoffset
        
        print(f"Need to move to new position: {detpos}")
        
        if do_move:
            umv(_detbox, detpos)
            return detx(do_move=False)
            
    return sdd_now
    
@typechecked
def sampleoffset(offset_in_mm : Optional[float] = None):
    if offset_in_mm is None:
        print("Sample offset: %.0f mm"%(float(lima_saxs_config.getFieldValue("UserSampleOffset"))*1000.))
    else:
        lima_saxs_config.setStaticField('UserSampleOffset', f"{offset_in_mm/1000:.03f}")
        sampleoffset()

# Choose the right SampleOffset according to calib
def _getSampleOffset():
	if calib.position in ('Out', 'unknown'):
		return lima_saxs_config.getFieldValue('UserSampleOffset')
	else:
		return lima_saxs_config.getFieldValue('CalibSampleOffset')


lima_saxs_config.setDynamicField('SampleOffset', _getSampleOffset)


# Configuration of dynamic fields in dahu metadata


# Zeros
scalers_counters_config.setDynamicField('HS32Z01',lambda: scalers.getCalibration('time')[0])
scalers_counters_config.setDynamicField('HS32Z02',lambda: scalers.getCalibration('pin3')[0])
scalers_counters_config.setDynamicField('HS32Z03',lambda: scalers.getCalibration('pin41')[0])
scalers_counters_config.setDynamicField('HS32Z04',lambda: scalers.getCalibration('pin42')[0])
scalers_counters_config.setDynamicField('HS32Z05',lambda: scalers.getCalibration('pin4')[0])
scalers_counters_config.setDynamicField('HS32Z06',lambda: scalers.getCalibration('pin6')[0])
scalers_counters_config.setDynamicField('HS32Z07',lambda: scalers.getCalibration('pin7')[0])
scalers_counters_config.setDynamicField('HS32Z08',lambda: scalers.getCalibration('pin8')[0])
scalers_counters_config.setDynamicField('HS32Z09',lambda: scalers.getCalibration('pin1')[0])

# Factors
scalers_counters_config.setDynamicField('HS32F01',lambda: scalers.getCalibration('time')[1])
scalers_counters_config.setDynamicField('HS32F02',lambda: scalers.getCalibration('pin3')[1])
scalers_counters_config.setDynamicField('HS32F03',lambda: scalers.getCalibration('pin41')[1])
scalers_counters_config.setDynamicField('HS32F04',lambda: scalers.getCalibration('pin42')[1])
scalers_counters_config.setDynamicField('HS32F05',lambda: scalers.getCalibration('pin4')[1])
scalers_counters_config.setDynamicField('HS32F06',lambda: scalers.getCalibration('pin6')[1])
scalers_counters_config.setDynamicField('HS32F07',lambda: scalers.getCalibration('pin7')[1])
scalers_counters_config.setDynamicField('HS32F08',lambda: scalers.getCalibration('pin8')[1])
scalers_counters_config.setDynamicField('HS32F09',lambda: scalers.getCalibration('pin1')[1])

# Gain
scalers_counters_config.setDynamicField('HS32G01',lambda: scalers.getCalibration('time')[2])
scalers_counters_config.setDynamicField('HS32G02',lambda: scalers.getCalibration('pin3')[2])
scalers_counters_config.setDynamicField('HS32G03',lambda: scalers.getCalibration('pin41')[2])
scalers_counters_config.setDynamicField('HS32G04',lambda: scalers.getCalibration('pin42')[2])
scalers_counters_config.setDynamicField('HS32G05',lambda: scalers.getCalibration('pin4')[2])
scalers_counters_config.setDynamicField('HS32G06',lambda: scalers.getCalibration('pin6')[2])
scalers_counters_config.setDynamicField('HS32G07',lambda: scalers.getCalibration('pin7')[2])
scalers_counters_config.setDynamicField('HS32G08',lambda: scalers.getCalibration('pin8')[2])
scalers_counters_config.setDynamicField('HS32G09',lambda: scalers.getCalibration('pin1')[2])

### COMMON
lima_common_config.setDynamicField('WaveLength', lambda: _wavelength.position*1e-10)

### SAXS
lima_saxs_config.setDynamicField('DetectorPosition', lambda: _detbox.position/1000)

### EIGER2
lima_eiger2_config.setDynamicField('BSize_1', lambda: eiger2.proxy.image_bin[0])
lima_eiger2_config.setDynamicField('BSize_2', lambda: eiger2.proxy.image_bin[1])
lima_eiger2_config.setDynamicField('SampleDistance', lambda: _detbox.position/1000+float(lima_eiger2_config.getFieldValue('DetboxOffset'))+float(lima_saxs_config.getFieldValue('SampleOffset')))
processing_eiger2_config.setDynamicField('regrouping_mask_filename', lambda: lima_eiger2_config.getFieldValue('MaskFilePath')+'/'+lima_eiger2_config.getFieldValue('MaskFileName'))
    
    
### WAXS
lima_waxs_config.setDynamicField('BSize_1', lambda: waxs.image.binning[0])
lima_waxs_config.setDynamicField('BSize_2', lambda: waxs.image.binning[1])
lima_waxs_config.setDynamicField('PSize_1', lambda: waxs.image.binning[0]*4.4e-5)
lima_waxs_config.setDynamicField('PSize_2', lambda: waxs.image.binning[1]*4.4e-5)
lima_waxs_config.setDynamicField('Dim_1', lambda: waxs.image.width)
lima_waxs_config.setDynamicField('Dim_2', lambda: waxs.image.height)
lima_waxs_config.setDynamicField('SampleDistance', lambda:float(lima_waxs_config.getFieldValue('UserSampleOffset')))
processing_waxs_config.setDynamicField('regrouping_mask_filename', lambda: lima_waxs_config.getFieldValue('MaskFilePath')+'/'+lima_waxs_config.getFieldValue('MaskFileName'))
processing_waxs_config.setDynamicField('dark_filename', lambda: lima_waxs_config.getFieldValue('DarkFilePath')+'/'+lima_waxs_config.getFieldValue('DarkFileName'))

#### BV
processing_bv_config.setDynamicField('dark_filename', lambda: lima_bv_config.getFieldValue('DarkFilePath')+'/'+lima_bv_config.getFieldValue('DarkFileName'))
lima_bv_config.setDynamicField('SampleDistance', lambda: _detbox.position/1000+float(lima_eiger2_config.getFieldValue('DetboxOffset'))+float(lima_saxs_config.getFieldValue('SampleOffset')))
 
    
    
    

