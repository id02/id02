from bliss.scanning import scan_meta
import json

def _getAttenuatorsStatus(scan=None):
    cfg = config.__wrapped__

    atts = dict()
    atts['att1'] = cfg.get('att1')
    atts['att2'] = cfg.get('att2')
    atts['att3'] = cfg.get('att3')
    atts['att4'] = cfg.get('att4')

    wcid02e = cfg.get('wcid02e')
    att5 = wcid02e.get('att5_0','att5_1','att5_2','att5_3')

    r = {}

    r['att5.0'] = { '@NX_class': 'NXattenuator',
                    'status': 'IN' if att5[0] else 'OUT',
                    'description': 'Aluminium 0.5mm'}

    r['att5.1'] = { '@NX_class': 'NXattenuator',
                    'status': 'IN' if att5[1] else 'OUT',
                    'description': 'Aluminium 1mm'}

    r['att5.2'] = { '@NX_class': 'NXattenuator',
                    'status': 'IN' if att5[2] else 'OUT',
                    'description': 'Aluminium 2mm'}

    r['att5.3'] = { '@NX_class': 'NXattenuator',
                    'status': 'IN' if att5[3] else 'OUT',
                    'description': 'Aluminium 4mm'}


    for a, att in atts.items():
        dval = {}
        dval['@NX_class'] = 'NXattenuator'
        dval['status'] = att.position

        for p in att.positions_list:
            if p['label'] == att.position:
                 dval['description'] = p['description']
 
        r[a] = dval

    return r

def _getDettubeInfo(scan=None):
    cfg = config.__wrapped__

    motors = ('detbox','detboxv','detboxh','dettabv','dettabh')

    r = dict()

    for m in motors:
        M = cfg.get(m)
        val = {'@NX_class': 'NXpositioner'}
        val['name'] = m
        val['value'] = M.position

        r[m] = val

    return r

def _getMirrorInfo(scan=None):
    cfg = config.__wrapped__

    motors = ('mirrz','mirty','mirty1','mirty2','mirtz1','mirtz2','mir2rz')

    r = dict()

    for m in motors:
        M = cfg.get(m)
        val = {'@NX_class': 'NXpositioner'}
        val['name'] = m
        val['value'] = M.position

        r[m] = val

    return r

def _getMonochromatorInfo(scan=None):
    cfg = config.__wrapped__
 
    motors = ('ccrot','cctra','cchgt','cctilt','wl','egy')

    r = dict()

    for m in motors:  
        M = cfg.get(m)
        val = {'@NX_class': 'NXpositioner'}
        val['name'] = m
        val['value'] = M.position

        r[m] = val

    return r

def _getMachineInfo(scan=None):
    cfg = config.__wrapped__

    motors = ('u21d','u21m','u35u')

    r = dict()

    r['srcur'] = {'@NX_class': 'NXsensor', 'name':'srcur','value': cfg.get('srcur').value }

    for m in motors:
        M = cfg.get(m)
        val = {'@NX_class': 'NXpositioner'}
        val['name'] = m
        val['value'] = M.position

        r[m] = val

    return r


def _getPrimarySlitInfo(scan=None):
    cfg = config.__wrapped__

    slits = ('p1','p2')

    r = dict()

    for s in slits:  
        Mvg = cfg.get(f"{s}vg")
        Mhg = cfg.get(f"{s}hg")
        Mvo = cfg.get(f"{s}vo")
        Mho = cfg.get(f"{s}ho")

        val = {'@NX_class': 'NXslit'} 
        val['name'] = s
        val['v_gap'] = Mvg.position
        val['h_gap'] = Mhg.position
        val['v_off'] = Mvo.position
        val['h_off'] = Mho.position

        r[s] = val

    return r

def _getSecondarySlitInfo(scan=None):
    cfg = config.__wrapped__

    slits = ('s1','s2','s3','s4','s5')

    r = dict()
   
    for s in slits:
        Mvg = cfg.get(f"{s}vg")
        Mhg = cfg.get(f"{s}hg")
        Mvo = cfg.get(f"{s}vo")
        Mho = cfg.get(f"{s}ho")

        val = {'@NX_class': 'NXslit'}
        val['name'] = s
        val['v_gap'] = Mvg.position
        val['h_gap'] = Mhg.position
        val['v_off'] = Mvo.position
        val['h_off'] = Mho.position

        r[s] = val

    return r



_instrobj = scan_meta.get_user_scan_meta().instrument

_instrobj.set('Attenuators', _getAttenuatorsStatus)

# No need if only motors: automatically done
#_instrobj.set('Dettube', _getDettubeInfo)
#_instrobj.set('Mirror',_getMirrorInfo)

lima_common_config.setDynamicField('AttenuatorsInfo',lambda : json.dumps(_getAttenuatorsStatus(None)))
lima_common_config.setDynamicField('DettubeInfo',lambda : json.dumps(_getDettubeInfo()))
lima_common_config.setDynamicField('MirrorInfo',lambda: json.dumps(_getMirrorInfo()))
lima_common_config.setDynamicField('MonochromatorInfo', lambda: json.dumps(_getMonochromatorInfo()))
lima_common_config.setDynamicField('MachineInfo', lambda: json.dumps(_getMachineInfo()))
lima_common_config.setDynamicField('PrimarySlitInfo', lambda: json.dumps(_getPrimarySlitInfo()))
lima_common_config.setDynamicField('SecondarySlitInfo', lambda: json.dumps(_getSecondarySlitInfo()))

