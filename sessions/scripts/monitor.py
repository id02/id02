
from .common import *


wagos = ('wcid02%s'%s for s in 'abcdefghi')

for w in wagos:
    if w not in globals():
        try:
            mon_temperature_mg.disable(w)
        except ValueError:
            pass

flint()

timescan(1, mon_pressure_mg, mon_temperature_mg, sleep_time=299)

