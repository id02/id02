
from .common import *
import time
import os
#from id02.common.data_policy import *

#data_policy_patch()

def newproposal(
    proposal_name = None,
    session_name = None,
    prompt = False,
):
    """
    Change the proposal and session name used to determine the saving path.
    """
    
    current_session.enable_esrf_data_policy()
    
    current_session.scan_saving.newproposal(
        proposal_name, session_name=session_name, prompt=prompt
    )
    
    dirn = os.path.dirname(current_session.scan_saving.data_fullpath)
    
    if os.path.isdir(dirn): # Already exist: just CD, otherwise, create dir
        os.chdir(dirn)
    else:
        newdataset()



def newcollection(
    collection_name: str
):
    """
    Change the collection name used to determine the saving path.
    Metadata can be modified later if needed.
    """
    
    current_session.scan_saving.newcollection(
        collection_name
    )
    
    newdataset()
    
    
def newsample(*args): newcollection(*args)
    
def newdataset():
    """
    Change the dataset name used to determine the saving path.
    """
    
    current_session.scan_saving.newdataset(time.strftime("%d%m%Y-%H%M%S"))
    
    dirn = os.path.dirname(current_session.scan_saving.data_fullpath)
    os.makedirs(dirn, 0o775, exist_ok=True)
    #os.makedirs(f"{dirn}/../../config", 0o775, exist_ok=True)
    os.chdir(dirn)

    
def endproposal():
    """
    Close the active dataset and move to the default inhouse proposal.
    """
    current_session.scan_saving.endproposal()
    current_session.disable_esrf_data_policy()

    os.chdir(os.path.dirname(current_session.scan_saving.data_fullpath))

