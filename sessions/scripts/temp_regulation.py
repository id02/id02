

from bliss.config.static import get_config
import gevent
from datetime import datetime

config = get_config()

def regulate(Tmin, Tmax):
    """
    Regulate the temperature of EH1/2
    By using the relay on wcid02f
    
    Close fully the valve below Tmin
    Open fully above Tmax
    Do duty cycle in between
    """
    
    relay_out = 'sample_rel2'
    
    sample_th1 = config.get('sample_th1')
    wcid02f    = config.get('wcid02f')

    duty_cycle = 0.5
    sleep_time = 1
    NMAX = 120

    while True:
        
        # Perform the duty cycle with a period of 2 min
        for i in range(NMAX):
            if i == 0 and duty_cycle < 1:
                wcid02f.set(relay_out, False)
            elif duty_cycle >= 1 or i > duty_cycle*NMAX:
                wcid02f.set(relay_out, True)
                
            gevent.sleep(1)
            
        T = sample_th1.value
        
        if T < Tmin:
            duty_cycle = 0
        elif T > Tmax:
            duty_cycle = 1
        else:
            duty_cycle = (T-Tmin)/(Tmax-Tmin)
                
        
        datenow = datetime.now().isoformat()
                
        print(f'{datenow}:  Current temperature: {T}C  New duty cycle: {duty_cycle}')
                
                
                
            

    


