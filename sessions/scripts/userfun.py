from typing import Optional, Union
from collections.abc import Sequence
from bliss.common.utils import modify_annotations

################################
## 
##  Acquisition
##
################################

@typechecked
def _to_bool(val : Union[bool, int, str]):
    if isinstance(val, bool):
        return val
    elif val == 0:
        return False
    elif val == 1:
        return True
    elif isinstance(val, str):
        val = val.lower()
        
        if val == "true":
            return True
        elif val == "false":
            return False
    
    raise ValueError("Cannot interpret the value {}")


def detexpose(exptime, save = False, title = "", HWTrigger = "", *, N_darks = 0):
	return saxsacq.detexpose(exptime, _to_bool(save), title, HWTrigger, N_darks=N_darks)

def detframes(nframes, clock, exptime, save = False, title = None, *, N_darks = 0):
	return saxsacq.detframes(nframes, clock, exptime, _to_bool(save), title, N_darks=N_darks)

@modify_annotations({"args": "dead0, dead, exptime, save=False, title='', HWTrigger=('', 'DI1' or 'DI2'), *, N_darks=0"})
def detmulti(nframes, deadtime0, deadtime, exptime, save = False, title = None, HWTrigger = "", *, N_darks = 0):
	return saxsacq.detmulti(nframes, deadtime0, deadtime, exptime, _to_bool(save), title, HWTrigger, N_darks=N_darks)

@modify_annotations({"args": "dead0, dead, dead_factor, exptime, save=False, title='', HWTrigger=('', 'DI1' or 'DI2'), *, N_darks=0"})
def detmvdc(nframes, dead0, dead, factor, exptime, save = False, title = None, HWTrigger = "", *, N_darks = 0):
	return saxsacq.detmvdc(nframes, dead0, dead, factor, exptime, _to_bool(save), title, HWTrigger, N_darks=N_darks)
	
def detmcal(nframes, dead0, dead, factor, exptime, showAll=False):
	saxsacq.detmcal(nframes, dead0, dead, factor, exptime, _to_bool(showAll))

@modify_annotations({"args": "dead0, [(N1, dead1),(N2,dead2),...,(Nn, deadn)], exptime, save=False, title='', HWTrigger=('', 'DI1' or 'DI2'), *, N_darks=0"})
def detkin(dead0, array_of_nframes_times, exptime, save = False, title = None, HWTrigger = "", *, N_darks = 0):
	return saxsacq.detkin(dead0, array_of_nframes_times, exptime, _to_bool(save), title, HWTrigger, N_darks=N_darks)

def detkcal(dead0, array_of_nframes_times, exptime, showAll=False):
	saxsacq.detkcal(dead0, array_of_nframes_times, exptime, _to_bool(showAll))

def dettrans(motor, start, stop, intervals, exptime, sleep_time = None, save = False, title = None, *, N_darks = 0):
	return saxsacq.dettrans(motor, start, stop, intervals, exptime, sleep_time, _to_bool(save), title, N_darks=N_darks)

def dettrans_rel(motor, start, stop, intervals, exptime, sleep_time = None, save = False, title = None, *, N_darks = 0):
	return saxsacq.dettrans_rel(motor, start, stop, intervals, exptime, sleep_time, _to_bool(save), title, N_darks=N_darks)


#################################
##
##  Acquisition info
##
#################################


def detexpinfo(detname, expert=False):
    
    if isinstance(detname, str):
        dnam = detname
    else:
        dnam = detname.name
    
    saxsacq.detexpinfo(dnam, expert)


#################################
##
##  Transmission setting
##
#################################

def dettrmsetup(I0 : int, I1 : int, time : int):
	trm_counter.trmsetup(I0, I1, time)


#################################
##
##  Calibration sample
##
#################################

def psin():
    """
    Insert 2um polystyrene calibration sample
    """
    
    calib.PS()

def pmmain():
    """
    Insert 400nm PMMA calibration sample
    """
    
    calib.PMMA()

def silverin():
    """
    Insert Silver Behanate calibration sample
    """
    
    calib.AgB()

def lupoin():
    """
    Insert Lupolen calibration sample
    """
    
    calib.Lupolen()

def calibsampleout():
    """
    Take out calibration sample
    """
    
    calib.Out()

def psout():
    """
    Take out calibration sample
    """
    
    calibsampleout()

def pmmaout():
    """
    Take out calibration sample
    """
    
    calibsampleout()

def silverout():
    """
    Take out calibration sample
    """
    
    calibsampleout()

def lupoout():
    """
    Take out calibration sample
    """
    
    calibsampleout()
	
##################################
##
##  Beamstop on frames
##
##################################

def framein(*frames):
    """
    Insert beamstops on frames.
    
    :param \\*frames: List of frames ids to be inserted.
    """
    
    framebs.framein(*frames)
    
def frameout():
    """
    Take out all beamstops on frames.
    """
    
    framebs.frameout()

##################################
##
##  Sample scan
##
##################################


def samplescan(motor, rfrom, rto, npoints, exptime):
    
    bvcl()
    fs_state = mcs.use_fast_shutter
    fson()
    flint()
    
    plotselect(motor.name, "mcs:pin7_raw")
    
    dscan(motor, rfrom, rto, npoints, exptime, mcs.counters.pin7_raw, save=False)
    
    if not fs_state:
        fsoff()
    
def samplescan6(motor, rfrom, rto, npoints, exptime):
    
    bvcl()
    fs_state = mcs.use_fast_shutter
    fson()
    flint()
    
    plotselect(motor.name, "mcs:pin6_raw")
    
    dscan(motor, rfrom, rto, npoints, exptime, mcs.counters.pin6_raw, save=False)
    
    if not fs_state:
        fsoff()


######################################
##
##  Select detector
##
######################################

def detactive(*dets):

    """
    Select 2D detectors
    """

    avail_detectors = ('eiger2','eiger500k','waxs','bv','cam')
    ddactive = []

    for d in dets:
        if hasattr(d, 'name'):
            d_ = d.name
        elif isinstance(d, str):
            d_ = d
        else:
            raise ValueError(f"Unknown object {d}")
        
        if d_ not in avail_detectors:
            raise ValueError(f"Detector {f} is not known.")

        ddactive += [d_, ]

    for d in avail_detectors:
        try:
            if d in ddactive:
                ACTIVE_MG.enable(f"{d}*")
            else:
                ACTIVE_MG.disable(f"{d}*")
        except:
            pass

    print(ACTIVE_MG.__info__())
