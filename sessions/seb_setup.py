
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")
load_script("seb.py")

DEFAULT_CHAIN.set_settings(default_chain_tfg['chain_config'])

print("")
print("Welcome to your new 'seb' BLISS session !! ")
print("")
print("You can now customize your 'seb' session by changing files:")
print("   * /seb_setup.py ")
print("   * /seb.yml ")
print("   * /scripts/seb.py ")
print("")
