
from bliss.setup_globals import *


load_script("default_scansaving_homeopid02.py")
ACTIVE_MG = mg_mcs
plotselect('mcs:pin4_raw')
flint()

load_script("shutters.py")

print("")
print("Welcome to your new 'shutters' BLISS session !! ")
print("")
print("You can now customize your 'shutters' session by changing files:")
print("   * /shutters_setup.py ")
print("   * /shutters.yml ")
print("   * /scripts/shutters.py ")
print("")
