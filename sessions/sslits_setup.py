
from bliss.setup_globals import *


load_script("default_scansaving_homeopid02.py")
ACTIVE_MG = mg_mcs
plotselect('mcs:pin41_raw')
flint()

load_script("sslits.py")

print("")
print("Welcome to your new 'sslits' BLISS session !! ")
print("")
print("You can now customize your 'sslits' session by changing files:")
print("   * /sslits_setup.py ")
print("   * /sslits.yml ")
print("   * /scripts/sslits.py ")
print("")
