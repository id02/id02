
from bliss.setup_globals import *

load_script("temp_regulation.py")

print("")
print("Welcome to your new 'temp_regulation' BLISS session !! ")
print("")
print("You can now customize your 'temp_regulation' session by changing files:")
print("   * /temp_regulation_setup.py ")
print("   * /temp_regulation.yml ")
print("   * /scripts/temp_regulation.py ")
print("")