
from bliss.setup_globals import *

load_script("test_eiger2.py")

print("")
print("Welcome to your new 'test_eiger2' BLISS session !! ")
print("")
print("You can now customize your 'test_eiger2' session by changing files:")
print("   * /test_eiger2_setup.py ")
print("   * /test_eiger2.yml ")
print("   * /scripts/test_eiger2.py ")
print("")