
from bliss.setup_globals import *

load_script("default_scansaving_homeopid02.py")
load_script("saxs.py")
load_script("tests.py")
load_script("userfun.py")
load_script("configdump.py")

DEFAULT_CHAIN.set_settings(default_chain_tfg['chain_config'])

print("")
print("Welcome to your new 'tests' BLISS session !! ")
print("")
print("You can now customize your 'tests' session by changing files:")
print("   * /tests_setup.py ")
print("   * /tests.yml ")
print("   * /scripts/tests.py ")
print("")
